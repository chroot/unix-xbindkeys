; --- brightness control ---
(xbindkey '(XF86MonBrightnessUp) "xbacklight -inc 5")
(xbindkey '(XF86MonBrightnessDown) "xbacklight -dec 5")
(xbindkey '(shift XF86MonBrightnessUp) "xbacklight -inc 10")
(xbindkey '(shift XF86MonBrightnessDown) "xbacklight -dec 10")
(xbindkey '(Pause) "slock") ; case matters ('pause' is illegal)
; +-- brightness control ---

; --- sound volume control ---
(xbindkey '(alt bracketleft) "amixer set Master 5%-")
(xbindkey '(alt shift bracketleft) "amixer set Master 10%-")
(xbindkey '(alt bracketright) "amixer set Master 5%+")
(xbindkey '(alt shift bracketright) "amixer set Master 10%+")
(xbindkey '(alt equal) "amixer set Master toggle")
; +-- sound volume control ---
